#include "graph_list.h"
int main(){
  int ros1,cols1,ros2,cols2;
  char description1[EDGE_SET_ITEM_LEN],description2[EDGE_SET_ITEM_LEN];
  int ro1,col1;
  int ro2,col2;
  int exp1,exp2;
  EdgeList *edge_list1,*edge_list2;
  List*all;
  fscanf(stdin,"%d",&ros1);
  fscanf(stdin,"%d",&cols1);
  fscanf(stdin,"%s",description1);
  fscanf(stdin,"%d",&ros2);
  fscanf(stdin,"%d",&cols2);
  fscanf(stdin,"%s",description2);
  fscanf(stdin,"%d",&ro1);
  fscanf(stdin,"%d",&col1);
  fscanf(stdin,"%d",&ro2);
  fscanf(stdin,"%d",&col2);
  fscanf(stdin,"%d",&exp1);
  fscanf(stdin,"%d",&exp2);
  edge_list1 = edge_list_make(ros1,cols1,description1);
  edge_list2 = edge_list_make(ros2,cols2,description2);
  all = list_make(ros1);
  edge_list_on(edge_list2,ro1,col1);
  edge_list_on(edge_list2,ro2,col2);
  list_all(all);
  edge_list_complement(edge_list1,edge_list2,all);
  assert(edge_list_is_on(edge_list1,ro1,exp1)==EDGE_SET_GAIN);
  assert(edge_list_is_on(edge_list1,ro1,exp2)==EDGE_SET_GAIN);
  assert(edge_list_is_on(edge_list1,ro1,col1)==EDGE_SET_FAIL);
  assert(edge_list_is_on(edge_list1,ro1,col2)==EDGE_SET_FAIL);
  list_print(edge_list1->table[ro1]);
  edge_list_destroy(&edge_list1);
  edge_list_destroy(&edge_list2);
  list_destroy(&all);
  return 0;
}
