#include "graph_list.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int from;
  float chance;
  EdgeList *edge_list;
  Q*q;
  Qdo*qdo;
  EDGE_SET_UINT32*path,*distance;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%f",&chance);
  srand(time(NULL));
  edge_list = edge_list_make(ros,cols,description);
  q = q_make(ros);
  qdo = qdo_make();
  path=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  edge_list_path(edge_list);
  edge_list_random(edge_list,chance);
  edge_list_bfs(edge_list,from,path,distance,q,qdo);
  assert(edge_set_is_connected(edge_list->ros,distance)==EDGE_SET_GAIN);
  edge_set_tree(edge_list->ros,distance,edge_set_max_distance(edge_list->ros,distance));
  edge_list_destroy(&edge_list);
  q_destroy(&q);
  qdo_destroy(&qdo);
  free(path);
  free(distance);
  return 0;
}
