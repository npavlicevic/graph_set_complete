#include "graph_list.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int from;
  int child,parent;
  int child_other,parent_other;
  EdgeList *edge_list;
  Q*q;
  Qdo*qdo;
  EDGE_SET_UINT32*path,*distance;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%d",&child);
  fscanf(stdin,"%d",&parent);
  fscanf(stdin,"%d",&child_other);
  fscanf(stdin,"%d",&parent_other);
  edge_list = edge_list_make(ros,cols,description);
  q = q_make(ros);
  qdo = qdo_make();
  path=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  edge_list_path(edge_list);
  edge_list_bfs(edge_list,from,path,distance,q,qdo);
  assert(path[child]==(EDGE_SET_UINT32)parent);
  assert(path[child_other]==(EDGE_SET_UINT32)parent_other);
  edge_set_tree(edge_list->ros,distance,edge_set_max_distance(edge_list->ros,distance));
  edge_list_destroy(&edge_list);
  q_destroy(&q);
  qdo_destroy(&qdo);
  free(path);
  free(distance);
  return 0;
}
