#ifndef GRAPH_LIST_H
#define GRAPH_LIST_H
#include "graph_set.h"
#include "graph_list_todos.h"
typedef struct list{
  EDGE_SET_UINT32 capacity;
  // available positions
  EDGE_SET_UINT32 count;
  // currently taken positions
  EDGE_SET_UINT32 *item;
  // array of items (hash codes)
}List;
List* list_make(EDGE_SET_UINT32 capacity);
void list_copy(List*this,List*one);
void list_print(List*this);
int list_contains(List*this,EDGE_SET_UINT32 item);
void list_union(List*this,List*one,List*other);
void list_intersect(List*this,List*one,List*other);
void list_difference(List*this,List*one,List*other);
void list_all(List*this);
void list_destroy(List** this);
typedef struct edge_list{
  List**table;
  // table (contains an array of lists)
  EDGE_SET_UINT32 ros;
  // number of ros
  EDGE_SET_UINT32 cols;
  // number of cols (or capacity of a list)
  char description[EDGE_SET_ITEM_LEN];
  // description
}EdgeList;
EdgeList* edge_list_make(EDGE_SET_UINT32 ros,EDGE_SET_UINT32 cols,char*description);
void edge_list_on(EdgeList*this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col);
void edge_list_off(EdgeList*this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col);
int edge_list_is_on(EdgeList *this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col);
List* edge_list_ro(EdgeList*this,EDGE_SET_UINT32 ro);
void edge_list_bellman(EdgeList *this, int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance);
void edge_list_bfs(EdgeList *this,int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance,Q* q, Qdo* qdo);
void edge_list_path(EdgeList*this);
void edge_list_random(EdgeList*this,float chance);
void edge_list_union(EdgeList*this,EdgeList*one,EdgeList*other);
void edge_list_intersect(EdgeList*this,EdgeList*one,EdgeList*other);
void edge_list_complement(EdgeList*this,EdgeList*one,List*all);
void edge_list_destroy(EdgeList**this);
#endif
