#include "graph_list.h"
/*
* Function: list_make
* 
* make list instance
*
* return: pointer to a list
*/
List* list_make(EDGE_SET_UINT32 capacity){
  List *this=(List*)calloc(1,sizeof(List));
  this->capacity=capacity;
  this->count=0;
  this->item=(EDGE_SET_UINT32*)calloc(capacity,sizeof(EDGE_SET_UINT32));
  return this;
}
/*
* Function: list_copy
* 
* copy a list
*
* return: nothing
*/
void list_copy(List*this,List*one){
  memcpy(this->item,one->item,sizeof(int)*(one->capacity));
  this->count=one->count;
}
/*
* Function: list_print
* 
* print a list
*
* return: nothing
*/
void list_print(List*this){
  EDGE_SET_UINT32 i;
  for(i=0;i<this->count;i++){
    printf("%d ",this->item[i]);
  }
  printf("\n");
}
/*
* Function: list_contains
* 
* check if list cointains item
*
* return: nothing
*/
int list_contains(List*this,EDGE_SET_UINT32 item){
  EDGE_SET_UINT32 i;
  for(i=0;i<this->count;i++){
    if(this->item[i]==item){
      return EDGE_SET_GAIN;
    }
  }
  return EDGE_SET_FAIL;
}
/*
* Function: list_union
* 
* make union of couple of ordered lists
*
* return: nothing
*/
void list_union(List*this,List*one,List*other){
  EDGE_SET_UINT32 i,j,k;
  k=0;
  for(i=0,j=0;i<one->count && j<other->count;){
    if(one->item[i]<other->item[j]){
      this->item[k]=one->item[i];
      i++;
    }else if(one->item[i]>other->item[j]){
      this->item[k]=other->item[j];
      j++;
    }else{
      this->item[k]=one->item[i];
      i++;
      j++;
    }
    k++;
  }
  for(;i<one->count;i++){
    this->item[k]=one->item[i];
    k++;
  } // only this loops runs
  for(;j<other->count;j++){
    this->item[k]=other->item[j];
    k++;
  } // or only this loop runs
  this->count=k;
}
/*
* Function: list_intersect
* 
* make union of couple of lists
*
* return: nothing
*/
void list_intersect(List*this,List*one,List*other){
  EDGE_SET_UINT32 i,j,k;
  k=0;
  for(i=0,j=0;i<one->count && j<other->count;){
    if(one->item[i]<other->item[j]){
      i++;
    }else if(one->item[i]>other->item[j]){
      j++;
    }else{
      this->item[k]=one->item[i];
      i++;
      j++;
      k++;
    }
  }
  this->count=k;
}
/*
* Function: list_difference
* 
* make difference of couple of lists
*
* return: nothing
*/
void list_difference(List*this,List*one,List*other){
  EDGE_SET_UINT32 i,j,k;
  k=0;
  for(i=0,j=0;i<one->count && j<other->count;){
    if(one->item[i]<other->item[j]){
      this->item[k]=one->item[i];
      i++;
      k++;
    }else if(one->item[i]>other->item[j]){
      j++;
    }else{
      i++;
      j++;
    }
  }
  for(;i<one->count;i++){
      this->item[k]=one->item[i];
      k++;
  }
  this->count=k;
}
/*
* Function: list_all
* 
* fill a list up to capacity
*
* return: nothing
*/
void list_all(List*this){
  EDGE_SET_UINT32 capacity,i;
  capacity=this->capacity;
  for(i=0;i<capacity;i++){
    this->item[i]=i;
  }
  this->count=capacity;
}
/*
* Function: list_destroy
* 
* destroy list instance
*
* return: nothing
*/
void list_destroy(List** this){
  free((*this)->item);
  free(*this);
  (*this)=NULL;
}
/*
* Function: edge_list_make
* 
* make instance of an edge list
*
* return: instance of an edge list
*/
EdgeList* edge_list_make(EDGE_SET_UINT32 ros,EDGE_SET_UINT32 cols,char*description){
  EdgeList*this=(EdgeList*)calloc(1,sizeof(EdgeList));EDGE_SET_UINT32 i;
  this->table=(List**)calloc(ros,sizeof(List*));
  for(i=0;i<ros;i++){
    this->table[i]=list_make(cols);
  }
  this->ros=ros;
  this->cols=cols;
  memcpy(this->description,description,strlen(description));
  return this;
}
/*
* Function: edge_list_on
* 
* create an edge (and keep edge list ordered)
*
* return: nothing
*/
void edge_list_on(EdgeList*this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col){
  List*this_l;EDGE_SET_UINT32 count;
  this_l=this->table[ro];
  count=this_l->count;
  this_l->item[count]=col;
  count++;
  this_l->count=count;
}
/*
* Function: edge_list_off
* 
* remove an edge
*
* return: nothing
*/
void edge_list_off(EdgeList*this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col){
  List *this_l;EDGE_SET_UINT32 i;
  this_l=this->table[ro];
  if(this_l->count<=0){
    return;
  }
  for(i=0;i<this_l->count;i++){
    if(this_l->item[i]==col){
      break;
    }
  }
  for(;i<this_l->count;i++){
    this_l->item[i]=this_l->item[i+1];
  }
  this_l->count--;
}
/*
* Function: edge_list_is_on
* 
* check if edge is on
*
* return: integer
*/
int edge_list_is_on(EdgeList *this,EDGE_SET_UINT32 ro,EDGE_SET_UINT32 col){
  List*this_l=this->table[ro];
  return list_contains(this_l,col);
}
/*
* Function: edge_list_ro
* 
* retreive a ro from a table
*
* return: List pointer
*/
List* edge_list_ro(EdgeList*this,EDGE_SET_UINT32 ro){
  return this->table[ro];
}
/*
* Function: edge_list_bellman
* 
* bellman ford algorithm for single source shortest path
*
* return: nothing
*/
void edge_list_bellman(EdgeList *this, int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance){
  EDGE_SET_UINT32 i,j,ros,updates,col,dist;
  List *this_l;
  ros=this->ros;
  for(i=0;i<ros;i++){
    distance[i]=INT_MAX;
  }
  distance[from]=0;
  updates=1;
  for(;updates==1;){
    updates=0;
    for(i=0;i<ros;i++){
      this_l=this->table[i];
      for(j=0;j<this_l->count;j++){
        col=this_l->item[j];
        dist=distance[i]+1;
        if(dist<distance[col]){
          distance[col]=dist;
          path[col]=i;
          updates=1;
        }
      }
    }
  }
}
/*
* Function: edge_list_bfs
* 
* breadth first search on an edge list
*
* return: nothing
*/
void edge_list_bfs(EdgeList *this,int from, EDGE_SET_UINT32 *path, EDGE_SET_UINT32 *distance,Q* q, Qdo* qdo){
  EDGE_SET_UINT32 i,ros,ro,col;
  List *this_l;
  ros=this->ros;
  for(i=0;i<ros;i++){
    distance[i]=INT_MAX;
  }
  distance[from]=0;
  qdo->q_push(q,from);
  for(;q->count>0;){
    ro=qdo->q_pop(q);
    this_l=this->table[ro];
    for(i=0;i<this_l->count;i++){
      col=this_l->item[i];
      if(distance[col]<INT_MAX){
        continue;
      }
      distance[col]=distance[ro]+1;
      path[col]=ro;
      qdo->q_push(q,col);
    }
  }
}
/*
* Function: edge_list_path
* 
* make a connected path
*
* return: nothing
*/
void edge_list_path(EdgeList*this){
  EDGE_SET_UINT32 i,ros;
  ros=this->ros;
  for(i=0;i<ros-1;i++){
    edge_list_on(this,i,i+1);
  }
}
/*
* Function: edge_list_random
* 
* make a random graph
*
* return: nothing
*/
void edge_list_random(EdgeList*this,float chance){
  float c;int i,j,ros;
  ros=this->ros;
  for(i=0;i<ros;i++){
    for(j=0;j<ros;j++){
      c=(float)(rand()%EDGE_SET_CHANCE_MAX);
      c=c/EDGE_SET_CHANCE_MAX;
      if(c>=chance) {
        edge_list_on(this,i,j);
      }
    }
  }
}
/*
* Function: edge_list_union
* 
* make an union of couple of edge lists
*
* return: nothing
*/
void edge_list_union(EdgeList*this,EdgeList*one,EdgeList*other){
  List*this_l,*one_l,*other_l;EDGE_SET_UINT32 i,ros;
  ros=this->ros;
  for(i=0;i<ros;i++){
    this_l=this->table[i];
    one_l=one->table[i];
    other_l=other->table[i];
    list_union(this_l,one_l,other_l);
  }
}
/*
* Function: edge_list_intersect
* 
* make an intersect of couple of edge lists
*
* return: nothing
*/
void edge_list_intersect(EdgeList*this,EdgeList*one,EdgeList*other){
  List*this_l,*one_l,*other_l;EDGE_SET_UINT32 i,ros;
  ros=this->ros;
  for(i=0;i<ros;i++){
    this_l=this->table[i];
    one_l=one->table[i];
    other_l=other->table[i];
    list_intersect(this_l,one_l,other_l);
  }
}
/*
* Function: edge_list_complement
* 
* make a complement of an edge list
*
* return: nothing
*/
void edge_list_complement(EdgeList*this,EdgeList*one,List*all){
  List*this_l,*one_l;EDGE_SET_UINT32 ros,i;
  ros=this->ros;
  for(i=0;i<ros;i++){
    this_l=this->table[i];
    one_l=one->table[i];
    list_difference(this_l,all,one_l);
  }
}
/*
* Function: edge_list_destroy
* 
* destroy instance of an edge list
*
* return: nothing
*/
void edge_list_destroy(EdgeList**this){
  List*this_l;EDGE_SET_UINT32 i;
  for(i=0;i<(*this)->ros;i++){
    this_l=(*this)->table[i];
    list_destroy(&this_l);
  }
  free((*this)->table);
  free(*this);
  *this=NULL;
}
