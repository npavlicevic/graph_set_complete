#include "table_test.h"

Test *test_create(int value) {
  Test *this = (Test*)calloc(1, sizeof(Test));
  this->value = value;
  return this;
}
void test_destroy(Test **this) {
  free(*this);
  *this = NULL;
}
void test_table_destroy(Table *table) {
  int i = 0;
  Test *test;
  for(; i < table->capacity; i++) {
    if(table->value[i] == NULL) {
      continue;
    }
    test = (Test*)table->value[i];
    test_destroy(&test);
  }
}
void test_insert_small() {
  int capacity = 2048;
  int i = 0;
  Table *table = table_create(capacity);
  Test *test;
  for(; i < capacity; i++) {
    test = test_create(rand() % capacity);
    table_insert(table, test, hash_mirror_int(&(test->value)));
    assert(table->value[test->value] != NULL);
  }
  test_table_destroy(table);
  table_destroy(&table);
}
