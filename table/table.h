#ifndef TABLE_H
#define TABLE_H
#include <stdlib.h>
#include <limits.h>
typedef struct table {
  int capacity;
  void **value;
} Table;

Table *table_create(int capacity);
void *table_insert(Table *this, void *value, int position);
int hash_mirror_int(void *key);
int hash_string(char *str, int len, int capacity);
void table_destroy(Table **this);
#endif
