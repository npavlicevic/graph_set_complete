#include "table.h"

/*
* Function: table_create
* 
* create table
*
* return: table
*/
Table *table_create(int capacity) {
  Table *this = (Table*)calloc(1, sizeof(Table));
  this->capacity = capacity;
  this->value = (void**)calloc(this->capacity, sizeof(void*));
  return this;
}
/*
* Function: table_insert
* 
* insert element into table
*
* return: inserted value
*/
void *table_insert(Table *this, void *value, int position) {
  if(this->value[position] != NULL) {
    return NULL;
  }
  this->value[position] = value;
  return value;
}
/*
* Function: hash_mirror_int
* 
* just mirror hash
*
* return: integer
*/
int hash_mirror_int(void *key) {
  int *temp = (int*)key;
  return *temp;
}
/*
* Function: hash_string
* 
* hash string
*
* return: integer
*/
int hash_string(char *str, int len, int capacity) {
  int i = 0;
  int hash = 0;
  int base = 128;

  if(capacity < 0) {
    capacity = USHRT_MAX;
  }

  for(;i < len;i++) {
    hash = base*hash + str[i];
    hash = hash % capacity;
  }

  return hash;
}
/*
* Function: table_destroy
* 
* destroy table
*
* return: nothing
*/
void table_destroy(Table **this) {
  free((*this)->value);
  free(*this);
  *this = NULL;
}
