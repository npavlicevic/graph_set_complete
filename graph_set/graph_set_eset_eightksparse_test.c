#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int from;
  float chance;
  EdgeSet *edge_set;
  Q*q;
  Qdo*qdo;
  EDGE_SET_UINT32*path,*distance;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%f",&chance);
  srand(time(NULL));
  edge_set = edge_set_make(ros,(cols/EDGE_SET_COLS_UNIT)+1,description);
  q = q_make(ros);
  qdo = qdo_make();
  path=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  edge_set_path(edge_set);
  edge_set_random(edge_set,chance);
  edge_set_bfs(edge_set,from,path,distance,q,qdo);
  assert(edge_set_is_connected(edge_set->ros,distance)==EDGE_SET_GAIN);
  edge_set_tree(edge_set->ros,distance,edge_set_max_distance(edge_set->ros,distance));
  edge_set_destroy(&edge_set);
  q_destroy(&q);
  qdo_destroy(&qdo);
  free(path);
  free(distance);
  return 0;
}
