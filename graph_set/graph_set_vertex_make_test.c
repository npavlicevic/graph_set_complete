#include "graph_set.h"
int main(){
  int hash_code;
  char type[EDGE_SET_ITEM_LEN];
  char title[EDGE_SET_ITEM_LEN];
  char item[EDGE_SET_ITEM_LEN];
  int index;
  Vertex *vertex;
  fscanf(stdin,"%d",&hash_code);
  fscanf(stdin,"%s",type);
  fscanf(stdin,"%s",title);
  fscanf(stdin,"%s",item);
  fscanf(stdin,"%d",&index);
  vertex = vertex_make();
  vertex_set(vertex,hash_code,type,title,item,index);
  assert((int)vertex->hash_code == hash_code);
  assert(strcmp(vertex->type,type) == 0);
  printf("%d %s\n",vertex->hash_code,vertex->type);
  vertex_destroy(&vertex);
  return 0;
}
