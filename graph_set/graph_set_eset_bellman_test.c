#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int pairs,i;
  int *ro,*col;
  EDGE_SET_UINT32 *distance,*path;
  int from;
  int vertex;
  int dist;
  EdgeSet *edge_set;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&pairs);
  ro=(int*)calloc(sizeof(int),pairs);
  col=(int*)calloc(sizeof(int),pairs);
  edge_set = edge_set_make(ros,(cols/EDGE_SET_COLS_UNIT)+1,description);
  for(i=0;i<pairs;i++) {
    fscanf(stdin,"%d",&ro[i]);
    fscanf(stdin,"%d",&col[i]);
    edge_set_on(edge_set,ro[i],col[i]);
  }
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%d",&vertex);
  fscanf(stdin,"%d",&dist);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(int),ros);
  path=(EDGE_SET_UINT32*)calloc(sizeof(int),ros);
  edge_set_bellman(edge_set,from,path,distance);
  assert(distance[vertex]==(EDGE_SET_UINT32)dist);
  printf("%d \n",distance[vertex]);
  free(ro);
  free(col);
  free(distance);
  free(path);
  edge_set_destroy(&edge_set);
  return 0;
}
