#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int from;
  int child,parent;
  int child_other,parent_other;
  EdgeSet *edge_set;
  Q*q;
  Qdo*qdo;
  EDGE_SET_UINT32*path,*distance;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%d",&child);
  fscanf(stdin,"%d",&parent);
  fscanf(stdin,"%d",&child_other);
  fscanf(stdin,"%d",&parent_other);
  edge_set = edge_set_make(ros,(cols/EDGE_SET_COLS_UNIT)+1,description);
  q = q_make(ros);
  qdo = qdo_make();
  path=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(EDGE_SET_UINT32),ros);
  edge_set_path(edge_set);
  edge_set_bfs(edge_set,from,path,distance,q,qdo);
  assert(path[child]==(EDGE_SET_UINT32)parent);
  assert(path[child_other]==(EDGE_SET_UINT32)parent_other);
  edge_set_tree(edge_set->ros,distance,edge_set_max_distance(edge_set->ros,distance));
  edge_set_destroy(&edge_set);
  q_destroy(&q);
  qdo_destroy(&qdo);
  free(path);
  free(distance);
  return 0;
}
