#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int ro1,col1;
  int ro2,col2;
  int exp1,exp2;
  int exp3,exp4;
  EdgeSet *edge_set;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&ro1);
  fscanf(stdin,"%d",&col1);
  fscanf(stdin,"%d",&ro2);
  fscanf(stdin,"%d",&col2);
  fscanf(stdin,"%d",&exp1);
  fscanf(stdin,"%d",&exp2);
  fscanf(stdin,"%d",&exp3);
  fscanf(stdin,"%d",&exp4);
  edge_set = edge_set_make(ros,ceil(cols/EDGE_SET_COLS_UNIT),description);
  edge_set_on(edge_set,ro1,col1);
  edge_set_on(edge_set,ro2,col2);
  assert((int)(edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT]) == (int)exp1);
  assert((int)(edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]) == (int)exp2);
  printf("%d %d\n",edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT],edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]);
  edge_set_off(edge_set,ro1,col1);
  edge_set_off(edge_set,ro2,col2);
  assert((int)(edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT]) == (int)exp3);
  assert((int)(edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]) == (int)exp4);
  printf("%d %d\n",edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT],edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]);
  edge_set_destroy(&edge_set);
  return 0;
}
