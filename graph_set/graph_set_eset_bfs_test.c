#include "graph_set.h"
int main(){
  int ros;
  int cols;
  char description[EDGE_SET_ITEM_LEN];
  int pairs,i;
  int *ro,*col;
  EDGE_SET_UINT32 *distance,*path;
  int from;
  int vertex;
  int dist,max_dist;
  int connected;
  EdgeSet *edge_set;
  Q*q;
  Qdo*qdo;
  fscanf(stdin,"%d",&ros);
  fscanf(stdin,"%d",&cols);
  fscanf(stdin,"%s",description);
  fscanf(stdin,"%d",&pairs);
  ro=(int*)calloc(sizeof(int),pairs);
  col=(int*)calloc(sizeof(int),pairs);
  edge_set = edge_set_make(ros,(cols/EDGE_SET_COLS_UNIT)+1,description);
  q=q_make(ros);
  qdo=qdo_make();
  for(i=0;i<pairs;i++) {
    fscanf(stdin,"%d",&ro[i]);
    fscanf(stdin,"%d",&col[i]);
    edge_set_on(edge_set,ro[i],col[i]);
  }
  fscanf(stdin,"%d",&from);
  fscanf(stdin,"%d",&vertex);
  fscanf(stdin,"%d",&dist);
  fscanf(stdin,"%d",&connected);
  distance=(EDGE_SET_UINT32*)calloc(sizeof(int),ros);
  path=(EDGE_SET_UINT32*)calloc(sizeof(int),ros);
  edge_set_bfs(edge_set,from,path,distance,q,qdo);
  assert(distance[vertex]==(EDGE_SET_UINT32)dist);
  printf("%d \n",distance[vertex]);
  assert(edge_set_is_connected(edge_set->ros,distance)==connected);
  printf("%d \n",connected);
  max_dist=edge_set_max_distance(edge_set->ros,distance);
  assert(max_dist==dist);
  edge_set_tree(edge_set->ros,distance,max_dist);
  free(ro);
  free(col);
  free(distance);
  free(path);
  edge_set_destroy(&edge_set);
  q_destroy(&q);
  qdo_destroy(&qdo);
  return 0;
}
