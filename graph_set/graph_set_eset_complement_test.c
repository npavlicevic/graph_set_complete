#include "graph_set.h"
int main(){
  int ros1,cols1,ros2,cols2;
  char description1[EDGE_SET_ITEM_LEN],description2[EDGE_SET_ITEM_LEN];
  int ro1,col1;
  int ro2,col2;
  int exp1,exp2;
  EdgeSet *edge_set1,*edge_set2;
  fscanf(stdin,"%d",&ros1);
  fscanf(stdin,"%d",&cols1);
  fscanf(stdin,"%s",description1);
  fscanf(stdin,"%d",&ros2);
  fscanf(stdin,"%d",&cols2);
  fscanf(stdin,"%s",description2);
  fscanf(stdin,"%d",&ro1);
  fscanf(stdin,"%d",&col1);
  fscanf(stdin,"%d",&ro2);
  fscanf(stdin,"%d",&col2);
  fscanf(stdin,"%d",&exp1);
  fscanf(stdin,"%d",&exp2);
  edge_set1 = edge_set_make(ros1,ceil(cols1/EDGE_SET_COLS_UNIT),description1);
  edge_set2 = edge_set_make(ros2,ceil(cols2/EDGE_SET_COLS_UNIT),description2);
  edge_set_on(edge_set2,ro1,col1);
  edge_set_on(edge_set2,ro2,col2);
  edge_set_complement(edge_set1,edge_set2);
  assert((int)(edge_set1->table[ro1][col1/EDGE_SET_COLS_UNIT]) == (int)exp1);
  assert((int)(edge_set1->table[ro2][col2/EDGE_SET_COLS_UNIT]) == (int)exp2);
  printf("%d %d\n",edge_set1->table[ro1][col1/EDGE_SET_COLS_UNIT],edge_set1->table[ro2][col2/EDGE_SET_COLS_UNIT]);
  edge_set_destroy(&edge_set1);
  edge_set_destroy(&edge_set2);
  return 0;
}
