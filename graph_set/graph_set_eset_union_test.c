#include "graph_set.h"
int main(){
  int ros1,cols1,ros2,cols2,ros3,cols3;
  char description1[EDGE_SET_ITEM_LEN],description2[EDGE_SET_ITEM_LEN],description3[EDGE_SET_ITEM_LEN];
  int ro1,col1;
  int ro2,col2;
  int exp1,exp2;
  EdgeSet *edge_set,*edge_set1,*edge_set2;
  fscanf(stdin,"%d",&ros1);
  fscanf(stdin,"%d",&cols1);
  fscanf(stdin,"%s",description1);
  fscanf(stdin,"%d",&ros2);
  fscanf(stdin,"%d",&cols2);
  fscanf(stdin,"%s",description2);
  fscanf(stdin,"%d",&ros3);
  fscanf(stdin,"%d",&cols3);
  fscanf(stdin,"%s",description3);
  fscanf(stdin,"%d",&ro1);
  fscanf(stdin,"%d",&col1);
  fscanf(stdin,"%d",&ro2);
  fscanf(stdin,"%d",&col2);
  fscanf(stdin,"%d",&exp1);
  fscanf(stdin,"%d",&exp2);
  edge_set = edge_set_make(ros3,ceil(cols3/EDGE_SET_COLS_UNIT),description3);
  edge_set1 = edge_set_make(ros1,ceil(cols1/EDGE_SET_COLS_UNIT),description1);
  edge_set2 = edge_set_make(ros2,ceil(cols2/EDGE_SET_COLS_UNIT),description2);
  edge_set_on(edge_set1,ro1,col1);
  edge_set_on(edge_set2,ro2,col2);
  edge_set_union(edge_set,edge_set1,edge_set2);
  assert((int)(edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT]) == (int)exp1);
  assert((int)(edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]) == (int)exp2);
  printf("%d %d\n",edge_set->table[ro1][col1/EDGE_SET_COLS_UNIT],edge_set->table[ro2][col2/EDGE_SET_COLS_UNIT]);
  edge_set_destroy(&edge_set);
  edge_set_destroy(&edge_set1);
  edge_set_destroy(&edge_set2);
  return 0;
}
