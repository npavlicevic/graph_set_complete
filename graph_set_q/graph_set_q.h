#ifndef GRAPH_SET_Q
#define GRAPH_SET_Q
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
typedef struct q{
  int capacity;
  // capacity of q
  int count;
  // number of items in q
  int first;
  // first element in q
  int last;
  // last element in q
  int *items;
  // items in q (hash codes)
}Q;
Q* q_make(int capacity);
void q_push(Q*this,int item);
int q_get(Q*this);
int q_pop(Q*this);
void q_destroy(Q **this);
typedef struct qdo{
  void (*q_push)(Q*,int);
  int (*q_get)(Q*);
  int (*q_pop)(Q*);
}Qdo;
Qdo* qdo_make();
void qdo_destroy(Qdo **this);
#endif
