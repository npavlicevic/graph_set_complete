#include "graph_set_q.h"

int main(){
  Q*q;
  Qdo*qdo;
  int count,i,item;
  int pop,expected;
  fscanf(stdin,"%d",&count);
  q=q_make(count);
  qdo=qdo_make();
  for(i=0;i<count;i++){
    fscanf(stdin,"%d",&item);
    qdo->q_push(q,item);
  }
  fscanf(stdin,"%d",&pop);
  for(i=0;i<pop;i++){
    qdo->q_pop(q);
  }
  fscanf(stdin,"%d",&expected);
  assert(qdo->q_get(q)==expected);
  q_destroy(&q);
  qdo_destroy(&qdo);
  return 0;
}
