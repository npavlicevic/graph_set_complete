#include "graph_set_q.h"
/*
* Function: q_make
* 
* make q instance
*
* return: pointer to a q
*/
Q* q_make(int capacity){
  Q*this=(Q*)calloc(sizeof(Q),1);
  this->capacity=capacity;
  this->count=0;
  this->first=0;
  this->last=0;
  this->items=(int*)calloc(sizeof(int),capacity);
  return this;
}
/*
* Function: q_push
* 
* add item to q
*
* return: nothing
*/
void q_push(Q*this,int item){
  this->count++;
  this->items[this->last]=item;
  this->last++;
  this->last=this->last%this->capacity;
}
/*
* Function: q_get
* 
* get item from q
*
* return: item from q
*/
int q_get(Q*this){
  return this->items[this->first];
}
/*
* Function: q_pop
* 
* remove item from q
*
* return: item from q
*/
int q_pop(Q*this){
  int tmp;
  this->count--;
  tmp=this->items[this->first];
  this->first++;
  this->first=this->first%this->capacity;
  return tmp;
}
/*
* Function: q_destroy
* 
* remove a q
*
* return: nothing
*/
void q_destroy(Q **this){
  free((*this)->items);
  free(*this);
  *this=NULL;
}
/*
* Function: qdo_make
* 
* make qdo instance
*
* return: pointer to a qdo
*/
Qdo* qdo_make(){
  Qdo*this=(Qdo*)calloc(sizeof(Qdo),1);
  this->q_push=q_push;
  this->q_get=q_get;
  this->q_pop=q_pop;
  return this;
}
/*
* Function: qdo_destroy
* 
* destroy qdo distance
*
* return: nothing
*/
void qdo_destroy(Qdo **this){
  free(*this);
  *this=NULL;
}
