### A collection of data structures to represent graphs alongside some of the algorithms.
  
  
Data structures included 
  
  
1. Edge set (as bit matrix)
2. Edge set (as a list)
3. Vertex set (as a hash table)
  
  
Algorithms included
  
  
1. Union (vertex set)
2. Intersect (vertex set)
3. Union (edge set)
4. Intersect (edge set)
5. Complement (edge set)
5. Connectivity (edge set breadth first search)
6. Shortest path from a single source (edge set bellman ford algorithm)
7. Random edge set
